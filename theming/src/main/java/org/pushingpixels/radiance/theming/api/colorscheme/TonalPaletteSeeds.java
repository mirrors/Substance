/*
 * Copyright (c) 2005-2025 Radiance Kirill Grouchnikov. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  o Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  o Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  o Neither the name of the copyright holder nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.pushingpixels.radiance.theming.api.colorscheme;

import java.awt.*;

public class TonalPaletteSeeds {
    public static final Color AQUA = new Color(112, 206, 239);
    public static final Color BARBY_PINK = new Color(238, 139, 230);
    public static final Color BOTTLE_GREEN = new Color(63, 181, 59);
    public static final Color BROWN = new Color(217, 179, 89);
    public static final Color CHARCOAL = new Color(110, 21, 27);
    public static final Color DESERT_SAND = new Color(182, 200, 119);
    public static final Color JADE_FOREST = new Color(21, 82, 25);
    public static final Color LIME_GREEN = new Color(169, 248, 57);
    public static final Color METALLIC = new Color(180, 185, 190);
    public static final Color OLIVE = new Color(175, 183, 142);
    public static final Color ORANGE = new Color(245, 200, 144);
    public static final Color PURPLE = new Color(203, 175, 237);
    public static final Color RASPBERRY = new Color(251, 110, 144);
    public static final Color SEPIA = new Color(195, 153, 128);
    public static final Color STEEL_BLUE = new Color(118, 165, 195);
    public static final Color SUNFIRE_RED = new Color(224, 20, 10);
    public static final Color SUN_GLARE = new Color(255, 255, 80);
    public static final Color SUNSET = new Color(255, 120, 41);
    public static final Color TERRACOTTA = new Color(239, 176, 105);
    public static final Color ULTRAMARINE = new Color(46, 22, 124);
    public static final Color VIOLET = new Color(107, 22, 124);
}
