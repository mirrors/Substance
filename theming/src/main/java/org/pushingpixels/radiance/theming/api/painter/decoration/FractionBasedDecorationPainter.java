/*
 * Copyright (c) 2005-2025 Radiance Kirill Grouchnikov. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  o Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer. 
 *     
 *  o Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution. 
 *     
 *  o Neither the name of the copyright holder nor the names of
 *    its contributors may be used to endorse or promote products derived 
 *    from this software without specific prior written permission. 
 *     
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package org.pushingpixels.radiance.theming.api.painter.decoration;

import org.pushingpixels.radiance.theming.api.RadianceSkin;
import org.pushingpixels.radiance.theming.api.RadianceThemingSlices;
import org.pushingpixels.radiance.theming.api.colorscheme.ContainerColorTokens;
import org.pushingpixels.radiance.theming.api.colorscheme.ContainerColorTokensSingleColorQuery;
import org.pushingpixels.radiance.theming.api.painter.FractionBasedPainter;
import org.pushingpixels.radiance.theming.internal.utils.RadianceColorUtilities;
import org.pushingpixels.radiance.theming.internal.utils.RadianceCoreUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.MultipleGradientPaint.CycleMethod;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Decoration painter with fraction-based stops and a color query associated
 * with each stop. This class allows creating multi-gradient decorations with
 * exact control over which color is used at every gradient control point.
 * 
 * @author Kirill Grouchnikov
 */
public class FractionBasedDecorationPainter extends FractionBasedPainter
		implements RadianceDecorationPainter {
	private Set<RadianceThemingSlices.DecorationAreaType> decoratedAreas;

	/**
	 * Creates a new fraction-based decoration painter.
	 *
	 * @param displayName
	 *            The display name of this painter.
	 * @param fractions
	 *            The fractions of this painter. Must be strictly increasing,
	 *            starting from 0.0 and ending at 1.0.
	 * @param colorQueries
	 *            The color queries of this painter. Must have the same size as
	 *            the fractions array, and all entries must be non-
	 *            <code>null</code>.
	 */
	public FractionBasedDecorationPainter(String displayName,
			float[] fractions, ContainerColorTokensSingleColorQuery[] colorQueries) {
		this(displayName, fractions, colorQueries,
				RadianceThemingSlices.DecorationAreaType.PRIMARY_TITLE_PANE,
				RadianceThemingSlices.DecorationAreaType.SECONDARY_TITLE_PANE);
	}

	/**
	 * Creates a new fraction-based decoration painter.
	 *
	 * @param displayName
	 *            The display name of this painter.
	 * @param fractions
	 *            The fractions of this painter. Must be strictly increasing,
	 *            starting from 0.0 and ending at 1.0.
	 * @param colorQueries
	 *            The color queries of this painter. Must have the same size as
	 *            the fractions array, and all entries must be non-
	 *            <code>null</code>.
	 * @param decorationAreas
	 *            Decoration areas that should be painted based on the color
	 *            queries. All the rest will be filled with a solid color from
	 *            the background color tokens of the matching decoration area.
	 */
	public FractionBasedDecorationPainter(String displayName,
			float[] fractions, ContainerColorTokensSingleColorQuery[] colorQueries,
			RadianceThemingSlices.DecorationAreaType... decorationAreas) {
		super(displayName, fractions, colorQueries);
		this.decoratedAreas = new HashSet<>();
		if (decorationAreas != null) {
            this.decoratedAreas.addAll(Arrays.asList(decorationAreas));
		}
	}

	@Override
	public void paintDecorationArea(Graphics2D graphics, Component comp,
			RadianceThemingSlices.DecorationAreaType decorationAreaType, int width, int height,
			RadianceSkin skin) {
		ContainerColorTokens colorTokens =
			skin.getBackgroundContainerTokens(decorationAreaType);
		if (this.decoratedAreas.contains(decorationAreaType)) {
			this.paintDecoratedBackground(graphics, comp, decorationAreaType,
					width, height, colorTokens);
		} else {
			this.paintSolidBackground(graphics, width, height, colorTokens);
		}
	}

    @Override
	public void paintDecorationArea(Graphics2D graphics, Component comp,
		RadianceThemingSlices.DecorationAreaType decorationAreaType, Shape contour,
		ContainerColorTokens colorTokens) {

		if (this.decoratedAreas.contains(decorationAreaType)) {
			this.paintDecoratedBackground(graphics, comp, decorationAreaType,
				contour, colorTokens);
		} else {
			this.paintSolidBackground(graphics, contour, colorTokens);
		}
	}

	private void paintDecoratedBackground(Graphics2D graphics, Component comp,
		RadianceThemingSlices.DecorationAreaType decorationAreaType, int width, int height,
		ContainerColorTokens colorTokens) {

		Graphics2D g2d = (Graphics2D) graphics.create();
		Color[] drawColors = new Color[this.fractions.length];
		for (int i = 0; i < this.fractions.length; i++) {
			ContainerColorTokensSingleColorQuery colorQuery = this.colorQueries[i];
			Color fromQuery = colorQuery.query(colorTokens);
			int alpha = this.alphas[i];
			int finalAlpha = fromQuery.getAlpha() * alpha / 255;
			Color finalColor = RadianceColorUtilities.getAlphaColor(fromQuery, finalAlpha);
			drawColors[i] = finalColor;
		}

		Component topMostWithSameDecorationAreaType = RadianceCoreUtilities
				.getTopMostParentWithDecorationAreaType(comp,
						decorationAreaType);
		Point inTopMost = SwingUtilities.convertPoint(comp, new Point(0, 0),
				topMostWithSameDecorationAreaType);
		int dy = inTopMost.y;

		MultipleGradientPaint gradient = new LinearGradientPaint(0, 0, 0,
			topMostWithSameDecorationAreaType.getHeight(), this.fractions,
			drawColors, CycleMethod.REPEAT);
		g2d.setPaint(gradient);
		g2d.translate(0, -dy);
		g2d.fillRect(0, 0, width, topMostWithSameDecorationAreaType.getHeight());

		g2d.dispose();
	}

	private void paintDecoratedBackground(Graphics2D graphics, Component comp,
		RadianceThemingSlices.DecorationAreaType decorationAreaType, Shape contour,
		ContainerColorTokens colorTokens) {

		Graphics2D g2d = (Graphics2D) graphics.create();
		Color[] drawColors = new Color[this.fractions.length];
		for (int i = 0; i < this.fractions.length; i++) {
			ContainerColorTokensSingleColorQuery colorQuery = this.colorQueries[i];
			Color fromQuery = colorQuery.query(colorTokens);
			int alpha = this.alphas[i];
			int finalAlpha = fromQuery.getAlpha() * alpha / 255;
			Color finalColor = RadianceColorUtilities.getAlphaColor(fromQuery, finalAlpha);
			drawColors[i] = finalColor;
		}

		Component topMostWithSameDecorationAreaType = RadianceCoreUtilities
				.getTopMostParentWithDecorationAreaType(comp,
						decorationAreaType);
		Point inTopMost = SwingUtilities.convertPoint(comp, new Point(0, 0),
				topMostWithSameDecorationAreaType);
		int dy = inTopMost.y;

		MultipleGradientPaint gradient = new LinearGradientPaint(0, 0, 0,
			topMostWithSameDecorationAreaType.getHeight(), this.fractions,
			drawColors, CycleMethod.REPEAT);
		g2d.setPaint(gradient);
		g2d.translate(0, -dy);
		g2d.fill(contour);

		g2d.dispose();
	}

	private void paintSolidBackground(Graphics2D graphics, int width, int height,
		ContainerColorTokens colorTokens) {

		graphics.setColor(colorTokens.getContainerSurface());
		graphics.fillRect(0, 0, width, height);
	}

	private void paintSolidBackground(Graphics2D graphics, Shape contour,
		ContainerColorTokens colorTokens) {

		graphics.setColor(colorTokens.getContainerSurface());
		graphics.fill(contour);
	}
}
