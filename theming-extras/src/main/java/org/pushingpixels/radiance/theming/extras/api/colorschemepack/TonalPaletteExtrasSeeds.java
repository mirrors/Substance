/*
 * Copyright (c) 2005-2025 Radiance Kirill Grouchnikov. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  o Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  o Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  o Neither the name of the copyright holder nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.pushingpixels.radiance.theming.extras.api.colorschemepack;

import java.awt.*;

public class TonalPaletteExtrasSeeds {
    public static final Color BELIZE = new Color(168, 221, 181);
    public static final Color BLOODY_MOON = new Color(252, 146, 114);
    public static final Color BLUE_YONDER = new Color(166, 189, 219);
    public static final Color BRICK_WALL = new Color(253, 187, 132);
    public static final Color BROWN_VELVET = new Color(190, 150, 118);
    public static final Color COBALT_STEEL = new Color(166, 189, 219);
    public static final Color DESERT_MARS = new Color(253, 174, 107);
    public static final Color EARTH_FRESCO = new Color(254, 192, 52);
    public static final Color EMERALD_GRASS = new Color(68, 148, 62);
    public static final Color FAUVE_MAUVE = new Color(239, 141, 206);
    public static final Color GOOSEBERRY_JUNGLE = new Color(158, 188, 218);
    public static final Color GREEN_PEARL = new Color(153, 216, 201);
    public static final Color MAHOGANY = new Color(206, 127, 154);
    public static final Color ORCHID_ALLOY = new Color(188, 189, 220);
    public static final Color PEACH = new Color(248, 131, 125);
    public static final Color PLACID_PINK = new Color(250, 159, 181);
    public static final Color SKY_HIGH = new Color(158, 202, 225);
    public static final Color SPRING_LEAF = new Color(161, 217, 155);
    public static final Color TURQUOISE_LAKE = new Color(64, 161, 196);
    public static final Color WILD_PINE = new Color(173, 221, 142);
    public static final Color YELLOW_MARINE = new Color(127, 205, 170);
}
